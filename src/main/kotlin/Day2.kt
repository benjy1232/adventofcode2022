class Day2(functions: Functions): Day {
    override val dayNumber: Int = 2
    override val lines: List<String>
    enum class Rps {
        ROCK, PAPER, SCISSORS
    }
    enum class Ltw{
        LOSE, TIE, WIN;
    }
    private val _yourMove: HashMap<Char, Rps>
    private val _opponentMove: HashMap<Char, Rps>


    init {
        val fileName = functions.resourceDir + "AdventDay2"
        lines = functions.getStringsFromFile(fileName)
        _yourMove = hashMapOf('X' to Rps.ROCK, 'Y' to Rps.PAPER, 'Z' to Rps.SCISSORS)
        _opponentMove = hashMapOf('A' to Rps.ROCK, 'B' to Rps.PAPER, 'C' to Rps.SCISSORS)
    }
    override fun part1() {
        var totalScore = 0
        for (line in lines) {
            val opponentChoice = _opponentMove[line[0]]
            val yourChoice = _yourMove[line[2]]
            var scoreThisRound: Int = yourChoice?.ordinal?.plus(1) ?: -3
            if (scoreThisRound < 0) {
                error("Score this round was negative")
            }

            if (yourChoice == opponentChoice) {
                scoreThisRound += 3
            } else {
                var pointThisRound = 0
                when (yourChoice) {
                    Rps.ROCK -> {
                        if (opponentChoice == Rps.SCISSORS) {
                            pointThisRound = 6
                        }
                    }
                    Rps.PAPER -> {
                        if (opponentChoice == Rps.ROCK) {
                            pointThisRound = 6
                        }
                    }
                    else ->{
                        if (opponentChoice == Rps.PAPER) {
                            pointThisRound = 6
                        }
                    }
                }
                scoreThisRound += pointThisRound
            }
            totalScore += scoreThisRound
        }
        println(totalScore)
    }

    override fun part2() {
        val loseWinTie = hashMapOf('X' to Ltw.LOSE, 'Y' to Ltw.TIE, 'Z' to Ltw.WIN)
        var totalScore = 0
        for (line in lines) {
            val opponentChoice = _opponentMove[line[0]]
            val yourChoice: Rps
            var scoreThisRound = 0
            when (loseWinTie[line[2]]!!) {
                Ltw.LOSE -> {
                    yourChoice = when (opponentChoice!!) {
                        Rps.ROCK -> Rps.SCISSORS
                        Rps.PAPER -> Rps.ROCK
                        Rps.SCISSORS -> Rps.PAPER
                    }
                }
                Ltw.TIE -> {
                    scoreThisRound = 3
                    yourChoice = opponentChoice!!
                }
                Ltw.WIN -> {
                    yourChoice = when (opponentChoice!!) {
                        Rps.ROCK -> Rps.PAPER
                        Rps.PAPER -> Rps.SCISSORS
                        Rps.SCISSORS -> Rps.ROCK
                    }
                    scoreThisRound = 6
                }
            }
            scoreThisRound += yourChoice.ordinal + 1
            totalScore += scoreThisRound
        }
        println(totalScore)
    }
}