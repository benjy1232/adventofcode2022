interface Day {
    val dayNumber: Int
    val lines: List<String>
    fun part1()
    fun part2()
}