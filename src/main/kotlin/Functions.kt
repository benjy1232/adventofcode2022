import java.io.File
class Functions(resourceDir: String) {
    val resourceDir: String
    init {
        this.resourceDir = resourceDir
    }
    fun getStringsFromFile(fileName: String): List<String> {
        val readStrings = mutableListOf<String>()
        File(fileName).forEachLine {
            readStrings.add(it)
        }
        return readStrings.toList()
    }
}