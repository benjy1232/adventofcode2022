class Day4(functions: Functions): Day {
    override val dayNumber: Int = 4
    override val lines: List<String>
    init {
        val fileName = functions.resourceDir + "AdventDay4"
        lines = functions.getStringsFromFile(fileName)
//        lines = listOf("2-4,6-8", "2-3,4-5", "5-7,7-9", "2-8,3-7", "6-6,4-6", "2-6,4-8")
    }
    override fun part1() {
        var totalNumContaining = 0
        for (line in lines) {
            val nums = parseInts(line)
            if (nums[0] < nums[2]) {
                if (nums[1] >= nums[3]) {
                    totalNumContaining++
                }
            } else if (nums[0] > nums[2]){
                if (nums[3] >= nums[1]) {
                    totalNumContaining++
                }
            } else {
                totalNumContaining++
            }
        }
        println(totalNumContaining)
    }

    // for overlapping there should be 4 in the test case
    override fun part2() {
        var totalOverlapping = 0
        for (line in lines) {
            val nums = parseInts(line)
            val firstSet = nums[0]..nums[1]
            for (num in nums[2]..nums[3]) {
                if (firstSet.contains(num)) {
                    totalOverlapping++
                    break
                }
            }
        }
        println(totalOverlapping)
    }

    private fun parseInts(line: String): List<Int> {
        val numStrings = line.split('-',',')
        val nums = mutableListOf<Int>()
        for (numString in numStrings) {
            nums.add(Integer.parseInt(numString))
        }
        return nums.toList()
    }
}