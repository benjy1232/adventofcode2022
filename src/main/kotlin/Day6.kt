class Day6(functions: Functions): Day {
    override val lines: List<String>
    init {
        val fileName = functions.resourceDir + "AdventDay6"
        lines = functions.getStringsFromFile(fileName)
    }
    override val dayNumber: Int
        get() = 6

    private fun solution(windowSize: Int) {
        val line = lines[0]
        var found = false
        line.windowed(windowSize).forEachIndexed { idx, it ->
            if (it.toCharArray().toSet().size == windowSize && !found) {
                found = true
                println(idx + windowSize)
            }
        }
    }
    override fun part1() {
        solution(4)
    }

    override fun part2() {
        solution(14)
    }
}