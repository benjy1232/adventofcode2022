fun main() {
    val os = System.getProperty("os.name")
    val resourceDirPath = if (os.contains("Windows")) {
        "C:\\Users\\bserr\\IdeaProjects\\adventofcode2022\\Resources\\"
    } else {
        "/home/bserrano/IdeaProjects/adventofcode2022/Resources/"
    }
    val functions = Functions(resourceDirPath)
    val days = mutableListOf<Day>()

    days.add(Day1(functions))
    days.add(Day2(functions))
    days.add(Day3(functions))
    days.add(Day4(functions))
    days.add(Day5(functions))
    days.add(Day6(functions))

    for (currentDay in days) {
        println("Day number: ${currentDay.dayNumber}")
        currentDay.part1()
        currentDay.part2()
    }
}