import java.util.*

class Day1(functions: Functions) : Day {
    override val dayNumber: Int = 1
    override val lines: List<String>

    init {
        val fileName = functions.resourceDir + "AdventDay1"
        lines = functions.getStringsFromFile(fileName)
    }

    override fun part1() {
        var maxCalories = 0
        var currentCalories = 0
        for (currentString in lines) {
            if (currentString.isEmpty() || currentString.isBlank()) {
                if (currentCalories > maxCalories) {
                    maxCalories = currentCalories
                }
                currentCalories = 0
                continue
            }
            currentCalories += currentString.toInt()
        }
        println(maxCalories)
    }

    override fun part2() {
        val pq = PriorityQueue(Collections.reverseOrder<Int>())
        var localCalories = 0
        for (currentLine in lines) {
            if (currentLine.isEmpty() || currentLine.isBlank()) {
                pq.add(localCalories)
                localCalories = 0
                continue
            }
            localCalories += currentLine.toInt()
        }
        var sum = 0
        for (i in 0..2) {
            sum += pq.remove()
        }
        println(sum)
    }
}