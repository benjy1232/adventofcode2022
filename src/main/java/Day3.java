import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Day3 implements Day{
    final List<String> _lines;
    public Day3(Functions functions) {
        String fileName = functions.getResourceDir() + "AdventDay3";
        _lines = functions.getStringsFromFile(fileName);
//        _lines = new ArrayList<>();
//        _lines.add("vJrwpWtwJgWrhcsFMMfFFhFp\n");
//        _lines.add("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\n");
//        _lines.add("PmmdzqPrVvPwwTWBwg\n");
//        _lines.add("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\n");
//        _lines.add("ttgJtRGJQctTZtZT\n");
//        _lines.add("CrZsJsPPZsGzwwsLwLmpwMDw");
    }
    @Override
    public int getDayNumber() { return 3; }

    @NotNull
    @Override
    public List<String> getLines() { return _lines; }

    @Override
    public void part1() {
        int totalSum = 0;
        for (String line: _lines) {
            for (String character: getCommon(line)) {
                totalSum += getValue(character.charAt(0));
            }
        }
        System.out.println(totalSum);
    }

    @Override
    public void part2() {
        int totalSum = 0;
        Set<String> common = new HashSet<>();
        for (int i = 0; i < _lines.size(); i++) {
            if (i % 3 == 0) {
                common = new HashSet<>(Arrays.asList(_lines.get(i).split("")));
                continue;
            } else if (i % 3 == 1) {
                common.retainAll(new HashSet<>(Arrays.asList(_lines.get(i).split(""))));
                continue;
            }
            common.retainAll(new HashSet<>(Arrays.asList(_lines.get(i).split(""))));
            common.remove("\n");
            for (String str: common) {
                totalSum += getValue(str.charAt(0));
            }
        }
        System.out.println(totalSum);
    }
    private int getValue(char character) {
        int value = character - 'A' + 1;
        if (value > 26) {
            value -= 32;
        } else {
            value += 26;
        }
        return value;
    }
    private Set<String> getCommon(String line) {
        int splitIdx = line.length() / 2;
        List<String> firstHalfArray = Arrays.asList(line.substring(0, splitIdx).split(""));
        List<String> secondHalfArray = Arrays.asList(line.substring(splitIdx).split(""));
        Set<String> firstHalf = new HashSet<>(firstHalfArray);
        Set<String> secondHalf = new HashSet<>(secondHalfArray);
        firstHalf.retainAll(secondHalf);
        return firstHalf;
    }
}
