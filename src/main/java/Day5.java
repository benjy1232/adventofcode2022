import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Day5 implements Day {
    final List<String> _lines;
    public Day5(Functions functions) {
        String resourcePath = functions.getResourceDir() + "AdventDay5";
        _lines = functions.getStringsFromFile(resourcePath);
//        _lines = new ArrayList<>();
//        _lines.add("    [D]    ");
//        _lines.add("[N] [C]    ");
//        _lines.add("[Z] [M] [P]");
//        _lines.add(" 1   2   3 ");
//        _lines.add("move 1 from 2 to 1");
//        _lines.add("move 3 from 1 to 3");
//        _lines.add("move 2 from 2 to 1");
//        _lines.add("move 1 from 1 to 2");
    }
    @Override
    public int getDayNumber() {
        return 5;
    }

    @NotNull
    @Override
    public List<String> getLines() {
        return _lines;
    }

    @Override
    public void part1() {
        HashMap<Integer, Stack<Character>> parsedColumns = parseColumns();
        for (String line: _lines) {
            if (!line.contains("move")) { continue; }
            List<Integer> instructions = getInstructions(line);
            Stack<Character> fromCol = parsedColumns.get(instructions.get(1));
            Stack<Character> toCol = parsedColumns.get(instructions.get(2));
            for (int i = 0; i < instructions.get(0); i++) {
                toCol.push(fromCol.pop());
            }
        }
        for (int i = 1; i <= parsedColumns.size(); i++) {
            Stack<Character> st = parsedColumns.get(i);
            System.out.print(st.peek());
        }
        System.out.println();
    }

    @Override
    public void part2() {
        HashMap<Integer, Stack<Character>> parsedColumns = parseColumns();
        for (String line: _lines) {
            if (!line.contains("move")) { continue; }
            List<Integer> instructions = getInstructions(line);
            Stack<Character> fromCol = parsedColumns.get(instructions.get(1));
            Stack<Character> tempColumn = new Stack<>();
            Stack<Character> toCol = parsedColumns.get(instructions.get(2));
            for (int i = 0; i < instructions.get(0); i++) {
                tempColumn.push(fromCol.pop());
            }
            while (!tempColumn.empty()) {
                toCol.push(tempColumn.pop());
            }
        }
        for (int i = 1; i <= parsedColumns.size(); i++) {
            Stack<Character> st = parsedColumns.get(i);
            System.out.print(st.peek());
        }
        System.out.println();
    }

    private HashMap<Integer, Stack<Character>> parseColumns() {
        // [<char>] [<char>] [<char>]
        HashMap<Integer, Stack<Character>> pc = new HashMap<>();
        for (String line: _lines) {
            if (!line.contains("[")) {
                break;
            }
            int startSearchIdx = 0;
            while (line.substring(startSearchIdx).contains("[")) {
                int idx = line.indexOf("[", startSearchIdx);
                int column = idx / 4 + 1;
                if (!pc.containsKey(column)) {
                    pc.put(column, new Stack<>());
                }
                Stack<Character> stack = pc.get(column);
                stack.push(line.charAt(idx + 1));
                startSearchIdx = idx + 1;
            }
        }

        for (int i = 1; i <= pc.size(); i++) {
            Stack<Character> toReverse = pc.get(i);
            Stack<Character> reversed = new Stack<>();
            int ogSize = toReverse.size();
            for (int j = 0; j < ogSize; j++) {
                reversed.push(toReverse.pop());
            }
            pc.put(i, reversed);
        }
        return pc;
    }

    private List<Integer> getInstructions(String line) {
        String[] ah = line.strip().split("move | from | to |\s");
        List<Integer> ret = new ArrayList<>();
        ret.add(Integer.parseInt(ah[1]));
        ret.add(Integer.parseInt(ah[2]));
        ret.add(Integer.parseInt(ah[3]));
        return ret;
    }
}
